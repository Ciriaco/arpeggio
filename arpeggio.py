#!/usr/bin/python3
# -*-coding:utf-8 -*
#
# Arpeggio is a tool to facilitate the learning of arpeggios on stringed instruments.
# https://framagit.org/Ciriaco/arpeggio
#
# Copyright (C) GPLv3 2020 Ciriaco
#

# Constants
DEBUG = False
DEFAULT_NBSTRINGS = 4
DEFAULT_NBFRETS = 24
DEFAULT_BOARD_FRET_WIDTH = 24
WIN_ROOT_TITLE = 'Arpeggio'
WIN_ROOT_WIDTH = 586
WIN_ROOT_HEIGHT = 160
WIN_FRETBOARD_TITLE = 'Fretboard'
WIN_FRETBOARD_WIDTH = 200
WIN_FRETBOARD_HEIGHT = 80 + DEFAULT_NBSTRINGS * 20
BOARD_FRET_OPEN_WIDTH = 8
BOARD_FRET_HEIGHT = 16
BOARD_WIDTH = BOARD_FRET_OPEN_WIDTH + DEFAULT_BOARD_FRET_WIDTH * DEFAULT_NBFRETS
BOARD_HEIGHT = BOARD_FRET_HEIGHT * DEFAULT_NBSTRINGS

# Global vars
gWinMain = None
gFretBoard = None
gTones = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B']

# Imports
import sys
if sys.version_info[0] < 3:
    raise OSError(WIN_ROOT_TITLE + ' needs python 3.')
try:
    import tkinter as tk
    from tkinter import ttk, messagebox
    import tkinter.font as tkFont
except ImportError as msg:
    print('Python 3 Tkinter not found.')
    sys.exit(-1)
from pprint import pprint


###
# Fretboard class
#
class cFretBoard():
    """ Fretboard class
        board:
            - semi-tons values : C=0, C#=1, ... B=11
            - list of lists : the strings composed of frets
            - fret 0 is open string
        canvas: an existing canvas widget
        chord: selected chord (structure of semi-tons value from the 'tone', -1 if disabled)
    """

    defaultTuning = [4,9,2,7,11,4]
    board = []
    nbfrets = DEFAULT_NBFRETS
    nbstrings = DEFAULT_NBSTRINGS
    canvas = None
    chord = {'tone':0, 'third':3, 'fifth':7, 'seventh':-1} # Cm by default
    garbage = []
    revertFlag = 0

    def __init__(self, canvas):

        self.canvas = canvas

        # compute default semi-tons into board
        del self.board[:]
        for string in range(0, self.nbstrings):
            # fret 0 is open-string
            frets = []
            frets.append(self.defaultTuning[string])
            # following frets...
            for fret in range(1, self.nbfrets + 1):
                frets.append((frets[fret - 1] + 1) % 12)
            self.board.append(frets)
        if DEBUG: pprint(self.board, width=82)

    def rebuild(self):
        if DEBUG: print('cFretBoard:rebuild')
        # before delete board, save open-string values
        openStrings = []
        for string in range(0, self.nbstrings):
            try:
                openStrings.append(self.board[string][0])
            except IndexError:
                openStrings.append(self.defaultTuning[string])

        # compute default semi-tons into board
        del self.board[:]
        for string in range(0, self.nbstrings):
            # fret 0 is open-string
            frets = []
            frets.append(openStrings[string])
            # following frets...
            for fret in range(1, self.nbfrets + 1):
                frets.append((frets[fret - 1] + 1) % 12)
            self.board.append(frets)
        if DEBUG: pprint(self.board, width=82)

    def retune(self):
        if DEBUG: print('cFretBoard:retune')
        for string in range(0, self.nbstrings):
            for fret in range(1, self.nbfrets + 1):
                self.board[string][fret] = (self.board[string][fret - 1] + 1) % 12
        if DEBUG: pprint(self.board, width=82)

    def draw(self):
        if DEBUG: print('cFretBoard:draw')

        # dynamic size of frets
        boardHeight = BOARD_FRET_HEIGHT * self.nbstrings
        fretHeight = BOARD_FRET_HEIGHT
        fretWidth = round((BOARD_WIDTH - BOARD_FRET_OPEN_WIDTH) / self.nbfrets)

        # resize canvas height 
        self.canvas.config(width=BOARD_WIDTH, height=boardHeight)
        self.canvas.grid(column=0, row=2, padx=0, pady=8, columnspan=3)

        # remove canvas subobjects
        for canobject in self.garbage: self.canvas.delete(canobject)

        # draw open strings
        for string in range(0, self.nbstrings):
            if self.revertFlag == 1:
                revertString = string
            else:
                revertString = self.nbstrings - 1 - string
            x1 = 0
            y1 = string * fretHeight
            x2 = BOARD_FRET_OPEN_WIDTH
            y2 = (string + 1) * fretHeight
            if self.board[revertString][0] == self.chord['tone']:
                self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill='#344966', outline='#344966'))
            elif self.board[revertString][0] == (self.chord['tone'] + self.chord['third']) % 12:
                self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill='#D63F44', outline='#D63F44'))
            elif self.board[revertString][0] == (self.chord['tone'] + self.chord['fifth']) % 12:
                self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill='#00BBF9', outline='#00BBF9'))
            elif self.chord['seventh'] != -1 and self.board[revertString][0] == (self.chord['tone'] + self.chord['seventh']) % 12:
                self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill='#00F5D4', outline='#00F5D4'))
            else:
                self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill='#d9d9d9', outline='#d9d9d9'))

        # draw board
        for string in range(0, self.nbstrings):
            for fret in range(1, self.nbfrets + 1):
                if self.revertFlag == 1:
                    revertString = string
                else:
                    revertString = self.nbstrings - 1 - string
                x1 = (fret - 1) * fretWidth + BOARD_FRET_OPEN_WIDTH
                y1 = string * fretHeight
                x2 = x1 + fretWidth
                y2 = y1 + fretHeight
                if self.board[revertString][fret] == self.chord['tone']:
                    color = '#344966'
                    self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill=color))
                    self.garbage.append(self.canvas.create_line(x1, y1, x2, y2, fill='#ffffff'))
                    self.garbage.append(self.canvas.create_line(x1+fretWidth, y1, x2-fretWidth, y2, fill='#ffffff'))
                elif self.board[revertString][fret] == (self.chord['tone'] + self.chord['third']) % 12:
                    color = '#D63F44'
                    self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill=color))
                elif self.board[revertString][fret] == (self.chord['tone'] + self.chord['fifth']) % 12:
                    color = '#00BBF9'
                    self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill=color))
                elif self.chord['seventh'] != -1 and self.board[revertString][fret] == (self.chord['tone'] + self.chord['seventh']) % 12:
                    color = '#00F5D4'
                    self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill=color))
                else:
                    color = '#a2846b'
                    self.garbage.append(self.canvas.create_rectangle(x1, y1, x2, y2, fill=color))

                # draw board landmarks
                fretmod = (fret - 1) % 12
                x = (fret - 1) * fretWidth + BOARD_FRET_OPEN_WIDTH + round(fretWidth / 2)
                y = string * fretHeight + round(fretHeight / 2)
                r = 4
                if ((self.nbstrings - string) == 2 and self.revertFlag == 0) or (string == 1 and self.revertFlag == 1):
                    if fretmod == 2 or fretmod == 4 or fretmod == 6 or fretmod == 8 or fretmod == 11:
                        self.garbage.append(self.canvas.create_oval(x-r, y-r, x+r, y+r, fill='#ffffff'))
                    if fretmod == 11 and self.revertFlag == 0:
                        y = y - fretHeight
                        self.garbage.append(self.canvas.create_oval(x-r, y-r, x+r, y+r, fill='#ffffff'))
                if fretmod == 11 and string == 3 and self.revertFlag == 1:
                    y = y - fretHeight
                    self.garbage.append(self.canvas.create_oval(x-r, y-r, x+r, y+r, fill='#ffffff'))

    def revert(self):
        if DEBUG: print('cFretBoard:revert', self.revertFlag)
        self.revertFlag = (self.revertFlag + 1) % 2
        self.draw()

###
# Fretboard parameters frame class
#
class cFretBoardFrame():
    """Fretboard parameters window"""

    widgetHeight = 0

    def __init__(self, master):
        self.master = master
        self.master.title(WIN_FRETBOARD_TITLE)
        self.master.resizable(False, False)
        self.master.geometry(str(WIN_FRETBOARD_WIDTH) + 'x' + str(WIN_FRETBOARD_HEIGHT))
        self.master.grab_set()

        # get widget height for dynamic window sizing
        self.lblTemp = tk.Label(master=self.master, text='temp')
        self.lblTemp.grid(column=0, row=0)
        self.lblTemp.update_idletasks()
        self.widgetHeight = self.lblTemp.winfo_height() + 2
        self.lblTemp.destroy()
        if DEBUG: print('cFretBoardFrame:__init__:widgetHeight', self.widgetHeight)

        # button
        self.btnValidation = tk.Button(master=master, command=self.__ebtnValidation, state=tk.NORMAL, text="OK", padx=0)

        # labels and entries
        self.lblNfrets = tk.Label(master=master, text='number of frets [12-24]', padx=0, pady=2)
        self.lblNfrets.grid(column=0, row=0)

        txtNfretsValidate = master.register(self.__etxtNbfrets)
        self.txtNfrets = tk.Entry(master=master, width=2)
        self.txtNfrets.insert(0, gFretBoard.nbfrets)
        self.txtNfrets.config(validate='key', validatecommand=(txtNfretsValidate, '%P'))
        self.txtNfrets.grid(column=1, row=0)

        self.lblNstrings = tk.Label(master=master, text='number of strings [1-6]', padx=0, pady=2)
        self.lblNstrings.grid(column=0, row=1)

        txtNstringsValidate = master.register(self.__etxtNbstrings)
        self.txtNstrings = tk.Entry(master=master, width=2)
        self.txtNstrings.insert(0, gFretBoard.nbstrings)
        self.txtNstrings.config(validate='key', validatecommand=(txtNstringsValidate, '%P'))
        self.txtNstrings.grid(column=1, row=1)

        # tuning widgets
        self.lblTones = []
        self.lstTones = []
        self.oldNstrings = 0
        self.__refreshToneWidgets()

    def __refreshToneWidgets(self):
        # dynamic labels and lists for strings tuning
        if DEBUG: print('cFretBoardFrame:__refreshToneWidgets')
        if gFretBoard.nbstrings > self.oldNstrings:
            for d in range(self.oldNstrings, gFretBoard.nbstrings):
                if DEBUG: print('cFretBoardFrame:add control',d)
                self.lblTones.append(tk.Label(master=self.master, text='tone of string ' + str(d+1), padx=0, pady=2))
                self.lblTones[d].grid(column=0, row=d+2)

                self.lstTones.append(ttk.Combobox(master=self.master, values=gTones, width=4, state='readonly'))
                self.lstTones[d].current(gFretBoard.board[d][0])
                self.lstTones[d].bind('<<ComboboxSelected>>', lambda event, index=d: self.____elstTones(event, index))
                self.lstTones[d].grid(column=1, row=d+2)

        elif gFretBoard.nbstrings < self.oldNstrings:
            for d in range(self.oldNstrings - 1, gFretBoard.nbstrings - 1, -1):
                if DEBUG: print('cFretBoardFrame:remove control',d)
                self.lblTones[d].destroy()
                del self.lblTones[d]

                self.lstTones[d].destroy()
                del self.lstTones[d]

        # store old number of strings for dynamic controls handling
        self.oldNstrings = gFretBoard.nbstrings

        # resize window
        self.master.geometry(str(WIN_FRETBOARD_WIDTH) + 'x' + str(80 + gFretBoard.nbstrings * self.widgetHeight))
        self.master.grab_set()

        # reposition button
        self.btnValidation.grid(column=0, row=gFretBoard.nbstrings+2, columnspan=2)

    # Nfrets selection
    def __etxtNbfrets(self, newValue):
        if DEBUG: print('cFretBoardFrame:__etxtNbfrets', newValue)
        try:
            newValue = int(newValue)
            if newValue >= 12 and newValue <= 24:
                gFretBoard.nbfrets = newValue
                gFretBoard.rebuild()
                self.btnValidation['state']= tk.NORMAL
                return True
            else:
                self.btnValidation['state']= tk.DISABLED
                return True
        except TypeError:
            self.btnValidation['state'] = tk.DISABLED
            return True
        except ValueError:
            self.btnValidation['state'] = tk.DISABLED
            return True

    # Nstrings selection
    def __etxtNbstrings(self, newValue):
        if DEBUG: print('cFretBoardFrame:__etxtNbstrings', newValue)
        try:
            newValue = int(newValue)
            if newValue >= 1 and newValue <= 6:
                self.oldNstrings = gFretBoard.nbstrings
                gFretBoard.nbstrings = newValue
                gFretBoard.rebuild()
                self.__refreshToneWidgets()
                self.btnValidation['state']= tk.NORMAL
                return True
            else:
                self.btnValidation['state']= tk.DISABLED
                return True
        except TypeError:
            self.btnValidation['state'] = tk.DISABLED
            return True
        except ValueError:
            self.btnValidation['state'] = tk.DISABLED
            return True

    # validation button
    def __ebtnValidation(self):
        gFretBoard.retune()
        gFretBoard.draw()
        self.master.destroy()

    # tone widget events
    def ____elstTones(self, event, index):
        # update open-string
        if DEBUG: print('cFretBoardFrame:tone value', index, self.lstTones[index].current())
        gFretBoard.board[index][0] = self.lstTones[index].current()

###
# Main frame class
#
class cMainFrame(tk.Frame):
    """ Main application window """

    def __init__(self, master=None):
        global gFretBoard

        super().__init__(master, width=WIN_ROOT_WIDTH, height=WIN_ROOT_HEIGHT)
        self.master.title(WIN_ROOT_TITLE)
        self.master.resizable(False, False)
        self.master.geometry(str(WIN_ROOT_WIDTH) + 'x' + str(WIN_ROOT_HEIGHT)) # force window expand
        self.grid(column=3, row=3)

        # font
        default_font = tkFont.nametofont("TkDefaultFont")
        default_font.configure(size=-12)
        self.master.option_add("*Font", default_font)

        # menu
        self.menu = tk.Menu(self)

        mnuMain = tk.Menu(self.menu, tearoff=0)
        mnuMain.add_command(label='Parameters', command=self.__emnuFretBoard)
        mnuMain.add_separator()
        mnuMain.add_command(label='Exit', command=self.__emnuExit)
        self.menu.add_cascade(label='Main', menu=mnuMain)

        mnuHelp = tk.Menu(self.menu, tearoff=0)
        mnuHelp.add_command(label='About', command=self.__emnuAbout)
        self.menu.add_cascade(label='?', menu=mnuHelp)

        self.master.config(menu=self.menu)

        # labels
        self.lblTone = tk.Label(self, text='Tone', padx=0, pady=0)
        self.lblTone.grid(column=0, row=0)
        self.lblChordType = tk.Label(self, text='Chord type', padx=0, pady=0)
        self.lblChordType.grid(column=1, row=0)

        # list
        self.lstTone = ttk.Combobox(self, values=gTones, width=4, state='readonly')
        self.lstTone.current(0)
        self.lstTone.bind('<<ComboboxSelected>>', self.__elstTone)
        self.lstTone.grid(column=0, row=1)

        lstChordTypes = ['m','m7','m7b5','maj','7','M7']
        self.lstChordType = ttk.Combobox(self, values=lstChordTypes, width=8, state='readonly')
        self.lstChordType.current(0)
        self.lstChordType.bind('<<ComboboxSelected>>', self.__elstChordType)
        self.lstChordType.grid(column=1, row=1)

        # canvas
        self.canBoard = tk.Canvas(self, width=BOARD_WIDTH, height=BOARD_FRET_HEIGHT*DEFAULT_NBSTRINGS, bg='blue')
        self.canBoard.grid(column=0, row=2, padx=0, pady=8, columnspan=3)

        # instanciate fretboard
        gFretBoard = cFretBoard(self.canBoard)

        # revert checkbox
        self.chkRevert = tk.Checkbutton(self, text='Revert', command=gFretBoard.revert)
        self.chkRevert.grid(column=2, row=0)

        # draw board
        gFretBoard.draw()

    # tone selection
    def __elstTone(self, event=None):
        if DEBUG: print('cMainFrame:__elstTone', self.lstTone.current())
        gFretBoard.chord['tone'] = self.lstTone.current()
        gFretBoard.draw()

    # chord type selection
    def __elstChordType(self, event=None):
        if DEBUG: print('cMainFrame:__elstChordType', self.lstChordType.current())
        if self.lstChordType.current() == 0:
            # minor
            gFretBoard.chord['third'] = 3
            gFretBoard.chord['fifth'] = 7
            gFretBoard.chord['seventh'] = -1
        elif self.lstChordType.current() == 1:
            # m7
            gFretBoard.chord['third'] = 3
            gFretBoard.chord['fifth'] = 7
            gFretBoard.chord['seventh'] = 10
        elif self.lstChordType.current() == 2:
            # m7b5
            gFretBoard.chord['third'] = 3
            gFretBoard.chord['fifth'] = 6
            gFretBoard.chord['seventh'] = 10
        elif self.lstChordType.current() == 3:
            # major
            gFretBoard.chord['third'] = 4
            gFretBoard.chord['fifth'] = 7
            gFretBoard.chord['seventh'] = -1
        elif self.lstChordType.current() == 4:
            # 7
            gFretBoard.chord['third'] = 4
            gFretBoard.chord['fifth'] = 7
            gFretBoard.chord['seventh'] = 10
        elif self.lstChordType.current() == 5:
            # M7
            gFretBoard.chord['third'] = 4
            gFretBoard.chord['fifth'] = 7
            gFretBoard.chord['seventh'] = 11
        gFretBoard.draw()

    # menu 'Parameters'
    def __emnuFretBoard(self):
        cFretBoardFrame(tk.Toplevel())
        
    # menu 'Exit'
    def __emnuExit(self):
        exit()

    # menu 'About'
    def __emnuAbout(self):
        if DEBUG: print('cMainFrame:__emnuAbout')
        messagebox.showinfo(title='About', message='This little tool is developed by Ciriaco. Have fun!')
        
###
# main()
#
if __name__ == "__main__":
    gWinMain = cMainFrame(tk.Tk())
    gWinMain.mainloop()
