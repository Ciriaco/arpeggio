# Arpeggio

Arpeggio is a little tool to facilitate the learning of arpeggios on stringed instruments. It is based on the simplest chords so as to provide a solid base from which the deduction of other chords is simple.

# Installation

Arpeggio requires python3 and tkinter.

### linux

```
$ sudo apt install python3.5 python3-tk git
```

```
$ git clone https://framagit.org/Ciriaco/arpeggio
$ cd arpeggio
$ chmod +x arpeggio.py
$ ./arpeggio.py
```

### Windows 10

Get and install python 3.8.5 from https://www.python.org/downloads/<br/>
Tkinter is with it.<br/>
<br/>
Download the project into a folder from https://framagit.org/Ciriaco/arpeggio/-/archive/master/arpeggio-master.zip<br/>
Double-click on arpeggio.py.

# License

Arpeggio is developped by Ciriaco and provided under the GNU General Public License (GPLv3).<br/>
See LICENSE.txt. You are required to provide any published modification of Arpeggio under a compatible license.
